@extends('dashboard.layout')
@section('header')
    Request Order
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('order.store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input class="form-control" id="nama_barang" name="nama_barang" type="text" >
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" id="jumlah" name="jumlah" type="number" >
                    </div>

                    <div class="tile-footer ">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
