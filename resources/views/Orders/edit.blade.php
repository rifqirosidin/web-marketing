@extends('dashboard.layout')
@section('header')
    Edit Order - {{ $orders->nama_barang }}
@endsection
@section('content-dashboard')

    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <form action="{{ route('order.update', $orders->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input class="form-control" value="{{ $orders->nama_barang }}" id="nama_barang" name="nama_barang" type="text"  placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input class="form-control" value="{{ $orders->jumlah }}"  id="jumlah" name="jumlah" type="text"  placeholder="Enter name">
                    </div>

                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
