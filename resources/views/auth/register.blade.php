@extends('auth.layouts.app')

@section('auth')

    <form class=" " action="{{ route('register') }}" method="POST">
        @csrf
        <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Register</h3>
        <div class="form-group">
            <label class="control-label">Name</label>
            <input class="form-control" type="text" name="name" placeholder="Name">
        </div>
        <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label class="control-label">Password</label>
            <input class="form-control" type="password" name="password" placeholder="password">
        </div>
        <div class="form-group">
            <label class="control-label">Konfirmasi Password</label>
            <input class="form-control" type="password" name="password_confirmation" placeholder="Password Corfirmation">
        </div>
        <div class="form-group btn-container">
            <button class="btn btn-primary btn-block"><i class="fa fa-lg fa-fw fa-user"></i>Register</button>
        </div>
        <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="{{ route('login') }}"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
        </div>
    </form>
@endsection

@push('js')
    <script>
        $(".login-box").css("width", "350px")
        $(".login-box").css("padding", "20px")
    </script>
@endpush
