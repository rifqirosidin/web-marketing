@extends('layouts.vali')
@section('title')
    Login Admin Produksi
@endsection
@section('dashboard')

    <section class="material-half-bg">
        <div class="cover"></div>
    </section>
    <section class="login-content">
        <div class="logo">
            <h1>Admin Marketing</h1>
        </div>
        <div class="login-box">
            @yield('auth')

        </div>
    </section>

@endsection

