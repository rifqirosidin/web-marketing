<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('request-orders', 'Api\OrderController@index');
Route::post('request-orders', 'Api\OrderController@store');
Route::get('request-orders/{id}/edit', 'Api\OrderController@edit');
Route::put('request-orders/update/{id}', 'Api\OrderController@update');
Route::delete('request-orders/{id}', 'Api\OrderController@destroy');


