<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CekOrderController extends Controller
{
    private $BASE_URL = "http://localhost:8080/api";
    public function index()
    {
        $client = new Client();

        $response = $client->get($this->BASE_URL."/orders");

        $responseData = json_decode($response->getBody(), true);
//        return $responseData;

        return view('cek_order_produksi.index', ['response' => $responseData['data']]);
    }
}
