<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->hasMany(Orders::class);
    }
}
