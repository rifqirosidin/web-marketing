<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->hasMany(Orders::class);
    }
}
